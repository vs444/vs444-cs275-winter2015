import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;


public class SmogCalc {
	//to test this with other twitter accounts, just change the value of the string variable below with the user name
	static String userName = "officialjaden";
	
	public static void main(String[] args) throws TembooException, IOException{
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		TembooSession session = new TembooSession("vs444", "myFirstApp", "67abfb694af14356a70f7a81861ab6c4");
		
		UserTimeline userTimelineChoreo = new UserTimeline(session);

		// Get an InputSet object for the choreo
		UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();

		// Set inputs
		userTimelineInputs.set_ScreenName(userName);
		userTimelineInputs.set_AccessToken("2941048179-TcX8tB2wJryDfpgf6jIVZ48uJ2JQdKqxhAOZ20U");
		userTimelineInputs.set_AccessTokenSecret("2KF0wUvydCt5j7pfbke7GP7YXZkucNqnKhzTHUJ1lmFoy");
		userTimelineInputs.set_ConsumerSecret("x3UZYtNW3XzQcHAjSulTXSJVS4CLfVDTcuPGERYfBtShSxwpIi");
		userTimelineInputs.set_ConsumerKey("K3moiCoOfe5K4QjVZKaw2V2S8");
		userTimelineInputs.set_Count("30");

		// Execute Choreo
		UserTimelineResultSet userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);
		JsonParser parser = new JsonParser();
		JsonArray userTimeLine = (JsonArray) parser.parse(userTimelineResults.get_Response());
		
		// Create ArrayLists to store values
		ArrayList<String> polyWords = new ArrayList<String>();
		String delims = "[ .,?!-#]+";
		
		//iterates through 30 tweets
		for(int i = 0; i < 30; i++){
			JsonObject tweets = userTimeLine.get(i).getAsJsonObject();
			String tweetText = tweets.get("text").getAsString();
			String[] tweetWords = tweetText.split(delims);
			
			//iterates through each word in a single tweet			
			for(int j = 0; j<tweetWords.length; j++)
			{
				
				if(tweetWords[j].split("/").length>1 || tweetWords[j].replaceAll("[^a-zA-Z -]", "").equals("") || tweetWords[j].length() == 0)
				{
					//does nothing if there are weird characters that will make their way into the URL
				}
				else
				{
					String sURL = "http://api.wordnik.com:80/v4/word.json/" + tweetWords[j] + "/hyphenation?useCanonical=true&limit=50&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5"; 
					URL url = new URL(sURL);
					HttpURLConnection request = (HttpURLConnection) url.openConnection();
					request.connect();
					
					// Convert to a JSON object to print data
					JsonParser jp = new JsonParser();
					JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
					JsonArray syllables = root.getAsJsonArray();
					
					//adds words with more than 2 syllables 
					if(syllables.size() > 2)
					{
						polyWords.add(tweetWords[j]);
					}
					
					
				}
			
			}			
			
		}
		System.out.println("There are "+polyWords.size()+" polysyllabic words. ");
		System.out.println("Here they are:");
		System.out.println(polyWords.toString());
		float grade = (float) (1.0430 * Math.sqrt(polyWords.size() * (30 / 30)) + 3.1291);
		System.out.println(userName+"'s SMOG grade is "+grade);
	} 
	
}
