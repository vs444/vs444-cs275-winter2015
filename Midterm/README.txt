To run the program, simply edit the value of the string userName in line 21 to the twitter username of the person you would like to
calculate a smog score for. You can leave it unedited to check @officialjaden

Here is the output from running it with @officialjaden

There are 9 polysyllabic words. 
Here they are:
[Really, Direction, Consistent, Forever, Experiment, Factory, Record, Passionate, Passionate]
officialjaden's SMOG grade is 6.2581
