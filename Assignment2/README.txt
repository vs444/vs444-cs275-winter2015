This app takes information from 3 SEPTA websites that return JSONs. You choose and source and destination from the drop down lists.
Press find trains to see what trains go from the given source to the given destinations. You will be taken to another screen that shows you the trains with some more informationb
If there are no trains, it will be blank. If you long press the train, you can see more detailed information about its location.

If you press the Show onMap button on the bottom right hand corner, you can see all of the trains as points on a google map.

If you zoom in too much on the map, the app crashes. Not sure how to fix that.

ALSO, I started the project in android studio in the middle of another project folder. For that reason, some of the names in the app folders are "WeatherApp". Please disregard that.