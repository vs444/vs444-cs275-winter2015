package com.example.vishnu.weatherapp;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class ListTrains extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_trains);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        TextView label = (TextView) findViewById(R.id.textView);
        String source = (String) getIntent().getExtras().get("source");
        String destination = (String) getIntent().getExtras().get("destination");
        label.setText(source+" to "+destination);

        String[] sourceSplit = source.split(" ");
        String[] destinationSplit = destination.split(" ");
        source = "";
        destination = "";
        for(String s : sourceSplit)
            source = source + s + "%20";
        for(String d : destinationSplit)
            destination = destination + d +"%20";
        source = source.substring(0,source.length()-3);
        destination = destination.substring(0,destination.length()-3);


        String sURL = "http://www3.septa.org/hackathon/NextToArrive/"+source+"/"+destination+"/15";
       // Connect to the URL
        try {
            URL url = new URL(sURL);

            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();
            JsonParser jp = new JsonParser();
            JsonArray root = (JsonArray) jp.parse(new InputStreamReader((InputStream) request.getContent()));
            final ArrayList<String> nextTrains = new ArrayList<String>();
            ArrayList<String> departTimes = new ArrayList<String>();
            ArrayList<String> arriveTimes = new ArrayList<String>();
            for(int i = 0; i<root.size(); i++){
                Log.v("meme", "got to the forleep");

                nextTrains.add(root.get(i).getAsJsonObject().get("orig_train").getAsString());
                departTimes.add(root.get(i).getAsJsonObject().get("orig_departure_time").getAsString());
                arriveTimes.add(root.get(i).getAsJsonObject().get("orig_arrival_time").getAsString());
            }

            ListView trainList = (ListView)findViewById(R.id.listView);
            String[] nextTrainsArr = new String[nextTrains.size()];
            String[] departTimesArr = new String[departTimes.size()];
            String[] arriveTimesArr = new String[arriveTimes.size()];

            nextTrainsArr = nextTrains.toArray(nextTrainsArr);
            departTimesArr = departTimes.toArray(departTimesArr);
            arriveTimesArr = arriveTimes.toArray(arriveTimesArr);

            String[] adapterInputs = new String[nextTrainsArr.length];
            for(int i = 0; i < adapterInputs.length; i++)
            {
                adapterInputs[i] = "Next Train To Arrive is Train No: "+nextTrains.get(i) +
                        " Departure Time: "+departTimes.get(i)+
                        " Arrival Time: "+arriveTimes.get(i);
            }

            ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, android.R.id.text1, adapterInputs);
            trainList.setAdapter(adapter);

            trainList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override

                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        Log.v("lele", "got to onitemlongclick");
                        String sURL = "http://www3.septa.org/hackathon/RRSchedules/" + nextTrains.get(position);
                        URL url = new URL(sURL);

                        HttpURLConnection request = (HttpURLConnection) url.openConnection();
                        request.connect();
                        JsonParser jp = new JsonParser();
                        JsonArray root = (JsonArray) jp.parse(new InputStreamReader((InputStream) request.getContent()));
                        JsonObject lastStop = null;
                        int counter = 0;
                        for(int i = 0; i < root.size(); i++){
                            if(root.get(i).getAsJsonObject().get("act_tm").getAsString().equals("na") && counter<1)
                            {
                                lastStop = root.get(i-1).getAsJsonObject();
                                counter++;

                            }
                        }
                        String trainDetails = "Last Departed from "+lastStop.get("station").getAsString()+" Scheduled to Depart That Station at "
                                +lastStop.get("sched_tm").getAsString()+" Actually Departed That Station at "+lastStop.get("act_tm");

                        Intent myIntent = new Intent(ListTrains.this, TrainDetails.class);
                        myIntent.putExtra("trainDetails", trainDetails);
                        startActivity(myIntent);

                        Button viewMap = (Button) findViewById(R.id.button2);
                        viewMap.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                try {
                                    String trainViewURL = "http://www3.septa.org/hackathon/TrainView/";
                                    URL url2 = new URL(trainViewURL);
                                    HttpURLConnection request2 = (HttpURLConnection) url2.openConnection();
                                    request2.connect();
                                    JsonParser parser = new JsonParser();
                                    JsonArray rootObj = (JsonArray) parser.parse(new InputStreamReader((InputStream) request2.getContent())).getAsJsonArray();

                                    String[] lats = new String[nextTrains.size()];
                                    String[] longs = new String[nextTrains.size()];

                                    for (
                                            int i = 0;
                                            i < rootObj.size(); i++)

                                    {
                                        for (int j = 0; j < lats.length; j++) {
                                            if (rootObj.get(i).getAsJsonObject().get("trainno").getAsString().equals(nextTrains.get(j))) {
                                                lats[j] = rootObj.get(i).getAsJsonObject().get("lat").getAsString();
                                                longs[j] = rootObj.get(i).getAsJsonObject().get("lon").getAsString();
                                            }
                                        }
                                    }

                                    Intent mapsView = new Intent(ListTrains.this, MapsActivity.class);
                                    mapsView.putExtra("Latitudes", lats);
                                    mapsView.putExtra("Longitudes", longs);
                                    mapsView.putExtra("Trains", nextTrains);

                                    startActivity(mapsView);
                                }
                                catch(Exception e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        })
;                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }
                    return true;
                }
            });



        }
        catch(Exception e)
        {
            e.printStackTrace();
        }




    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_trains, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
