package com.example.vishnu.weatherapp;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //edit Strict Mode
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayList<String> inputs = readDatCSV();
        inputs.remove(0);
        inputs.add(0,"Source");
        String[] adapterInputs = new String[inputs.size()];
        adapterInputs = inputs.toArray(adapterInputs);
        ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, adapterInputs);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayList<String> inputs2 = readDatCSV();
        inputs.remove(0);
        inputs.add(0,"Destination");
        String[] adapterInputs2 = new String[inputs2.size()];
        adapterInputs = inputs.toArray(adapterInputs2);
        ArrayAdapter<String> adapter2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, adapterInputs);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner2.setAdapter(adapter2);
    }

    public ArrayList<String> readDatCSV()
    {
        String csvFile = "station_id_name.csv";
        BufferedReader br;
        String line = "";
        String cvsSplitBy = ",";
        String[] information;
        ArrayList<String> stationNames = new ArrayList<String>();
        try {
            AssetManager am = getAssets();
            InputStream inpooter = am.open(csvFile);
            br = new BufferedReader(new InputStreamReader(inpooter));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                information = line.split(cvsSplitBy);
                for(int i = 1; i< information.length; i=i+2)
                    stationNames.add(information[i]);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stationNames;
    }

    public void findTrains(View v){
        Intent myIntent = new Intent(MainActivity.this, ListTrains.class);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);
        myIntent.putExtra("source", spinner.getSelectedItem().toString());
        myIntent.putExtra("destination", spinner2.getSelectedItem().toString());
        MainActivity.this.startActivity(myIntent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Log.v("ye", "hello");
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
