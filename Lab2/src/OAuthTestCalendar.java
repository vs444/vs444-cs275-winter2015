import java.io.InputStreamReader;
import java.net.*;
import java.util.Scanner;

import com.google.gson.*;

import java.io.*;


public class OAuthTestCalendar {
	public static String executePost(String targetURL, String urlParameters)
	{
		URL url;
		HttpURLConnection connection = null;  
		try {
			//Create connection
			url = new URL(targetURL);
			connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", 
					"application/x-www-form-urlencoded");

			connection.setRequestProperty("Content-Length", "" + 
					Integer.toString(urlParameters.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");  

			connection.setUseCaches (false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			//Send request
			DataOutputStream wr = new DataOutputStream (
					connection.getOutputStream ());
			wr.writeBytes (urlParameters);
			wr.flush ();
			wr.close ();
			//Get Response   
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer(); 
			while((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			return response.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if(connection != null) {
				connection.disconnect(); 
			}
		}
	}

	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
		//https://accounts.google.com/o/oauth2/auth?
		//		 		  scope=calendar&
		//				  redirect_uri=urn:ietf:wg:oauth:2.0:oob&
		//				  response_type=code&
		//				  client_id=93608742918-k8mbgsso30509djh5n6j5viejba98vlk.apps.googleusercontent.com
		//		https://accounts.google.com/o/oauth2/auth?scope=http://www.google.com/calendar/feeds/&redirect_uri=urn:ietf:wg:oauth:2.0:oob&response_type=code&client_id=93608742918-k8mbgsso30509djh5n6j5viejba98vlk.apps.googleusercontent.com	}

		//ww.googleapis.com/oauth2/v3/token%20HTTP/1.1?code=" + accessCode + "
		//4/HSVi3XbCUzFA9CVeQqvFM-G7cBEqsdAGTwC-88v_s1o.0k03T1URxwgZJvIeHux6iLY55SHWlgI
		Scanner in = new Scanner(System.in);
		System.out.println("provide client ID");
		String cID = in.nextLine();
		System.out.println("follow this link:");
		String getAccessKey = ("https://accounts.google.com/o/oauth2/auth?scope=http://www.google.com/calendar/feeds/&redirect_uri=urn:ietf:wg:oauth:2.0:oob&response_type=code&client_id=" + cID);
		System.out.println(getAccessKey);
		System.out.println("...And paste the code you are given:");
		String accessCode = in.nextLine();

		JsonParser parser = new JsonParser();
		JsonObject authorizeResponse = (JsonObject)parser.parse(executePost("https://accounts.google.com/o/oauth2/token", "code=" + accessCode + "&client_id=93608742918-k8mbgsso30509djh5n6j5viejba98vlk.apps.googleusercontent.com&client_secret=XBtek_8gCkziYgm6IDjy5ol1&redirect_uri=urn:ietf:wg:oauth:2.0:oob&grant_type=authorization_code"));

		String accessT =  authorizeResponse.get("access_token").getAsString();

		String calendarsListURL="https://www.googleapis.com/calendar/v3/users/me/calendarList" + "?access_token=" + accessT;
		URL url = new URL(calendarsListURL);
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.connect();

		// Convert to a JSON object
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		JsonObject rootobj = root.getAsJsonObject();
		JsonArray calendars = rootobj.get("items").getAsJsonArray();

		
		
		String firstCalendarID = "";
		//loop through each our and print the important information
		for(int i = 0; i < calendars.size(); i++){
			JsonObject calendar = calendars.get(i).getAsJsonObject();
			String calendarName = calendar.get("summary").getAsString();
			if(i == 0){
				firstCalendarID = calendar.get("id").getAsString();
			}
			System.out.println(calendarName);
		}

		
		
		String firstCalAccess = "https://www.googleapis.com/calendar/v3/calendars/" + firstCalendarID + "/events?access_token=" + accessT;
		url = new URL(firstCalAccess);
		request = (HttpURLConnection) url.openConnection();
		request.connect();

		// Convert to a JSON object
		jp = new JsonParser();
		root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		rootobj = root.getAsJsonObject();
		JsonArray events = rootobj.get("items").getAsJsonArray();

		for(int i = 0; i < events.size(); i++){
			JsonObject event = events.get(i).getAsJsonObject();
			String eventName = event.get("summary").getAsString();
			String eventTime = event.get("start").getAsJsonObject().get("dateTime").getAsString();
			System.out.println(eventTime + " - " + eventName);
		}
	}
}
