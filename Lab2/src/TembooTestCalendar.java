import java.util.Scanner;

import com.google.gson.*;
import com.temboo.Library.Google.Calendar.*;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsInputSet;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsResultSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsInputSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsResultSet;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.*;
import com.temboo.Library.Google.OAuth.FinalizeOAuth;
import com.temboo.Library.Google.OAuth.InitializeOAuth;
import com.temboo.Library.Google.OAuth.InitializeOAuth.*;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;




public class TembooTestCalendar {

	public static void main(String args[]) throws TembooException{
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		//TembooSession session = new TembooSession("vs444", "Temboo", "67abfb694af14356a70f7a81861ab6c4");

		TembooSession session = new TembooSession("vs444", "myFirstApp", "67abfb694af14356a70f7a81861ab6c4");
		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set inputs
		initializeOAuthInputs.set_ClientID("1002801502525-gbg8qtsvbp7tk460471tup2c4tlc5r56.apps.googleusercontent.com");
		initializeOAuthInputs.set_Scope("https://www.googleapis.com/auth/calendar");
	

		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
		String accessURL = initializeOAuthResults.get_AuthorizationURL();

		System.out.println("follow this link and authorize, then type 'done' to continue");
		System.out.println(accessURL);
		Scanner prompt = new Scanner(System.in);
		String done = prompt.nextLine();
		
		String callback_id = initializeOAuthResults.get_CallbackID();

		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_CallbackID(callback_id);
		finalizeOAuthInputs.set_ClientSecret("GMpynCMpq3Db-B8jrEvOaDXe");
		finalizeOAuthInputs.set_ClientID("1002801502525-gbg8qtsvbp7tk460471tup2c4tlc5r56.apps.googleusercontent.com");

		// Execute Choreo
		FinalizeOAuthResultSet results = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		
		String accessT = results.get_AccessToken();
		GetAllCalendars getAllCalendarsChoreo = new GetAllCalendars(session);

		// Get an InputSet object for the choreo
		GetAllCalendarsInputSet inputs = getAllCalendarsChoreo.newInputSet();

		// Set inputs
		inputs.set_AccessToken(accessT);
		inputs.set_ClientID("1002801502525-gbg8qtsvbp7tk460471tup2c4tlc5r56.apps.googleusercontent.com");
		inputs.set_ClientSecret("GMpynCMpq3Db-B8jrEvOaDXe");
		
		// Execute Choreo
		GetAllCalendarsResultSet resultset = getAllCalendarsChoreo.execute(inputs);
		String response = resultset.get_Response();
		
		JsonParser parser = new JsonParser();
		JsonArray calendars = (JsonArray)((JsonObject)parser.parse(response)).get("items").getAsJsonArray();
		
		String firstCalendarID = "";
		//loop through each our and print the important information
		for(int i = 0; i < calendars.size(); i++){
			JsonObject calendar = calendars.get(i).getAsJsonObject();
			String calendarName = calendar.get("summary").getAsString();
			if(i == 0){
				firstCalendarID = calendar.get("id").getAsString();
			}
			System.out.println(calendarName);
		}
		
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		// TembooSession session = new TembooSession("vs444", "myFirstApp", "67abfb694af14356a70f7a81861ab6c4");

		GetAllEvents getAllEventsChoreo = new GetAllEvents(session);

		// Get an InputSet object for the choreo
		GetAllEventsInputSet getAllEventsInputs = getAllEventsChoreo.newInputSet();

		// Set inputs
		getAllEventsInputs.set_AccessToken(accessT);
		getAllEventsInputs.set_CalendarID(firstCalendarID);
		// Execute Choreo
		GetAllEventsResultSet getAllEventsResults = getAllEventsChoreo.execute(getAllEventsInputs);
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(getAllEventsResults.get_Response());
		JsonObject rootobj = root.getAsJsonObject();
		
		JsonArray events = rootobj.get("items").getAsJsonArray();

		for(int i = 0; i < events.size(); i++){
			JsonObject event = events.get(i).getAsJsonObject();
			String eventName = event.get("summary").getAsString();
			String eventTime = event.get("start").getAsJsonObject().get("dateTime").getAsString();
			System.out.println(eventTime + " - " + eventName);
		}


	}
}
