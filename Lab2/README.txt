Lab 2 
Students: James "jes432" Swanick and Vishnu "vs444" Sreenivasan

The Manual and Temboo-managed versions of this program use essentially the same procedure to display the calendar information. 
The only differences lie in the way that OAuth is performed, so we will first describe the basic procedure and then describe the
differences. 

1. The program performs Oauth by prompting the user to give the program access by following a URL.
2. After OAuth is performed, the program acquires an access token using the access key that has been generated. 
3. Using the access token the program requests a jsonObject containing a list of calendars owned by the users' google account.
4. The program parses through the Json Object to list the names of the calendars, after saving the unique id of the first calendar. 
5. using the id collected above, the program requests a list of events in the first calendar as a json Object.
6. After receiving the Json Object containing the events, the program iterates through the array and collects the dates and names of
each event, printing them each time. 


-------   The  Differences   -----------

The main difference between the programs is the way they conduct the OAuth process. The first program requires the user to input
all of the information (client id, client secret, etc) one by one and constructs the HTTP requests manually with that information. 
The temboo method of the OAuth process replaces the manual construction of the requests by wrapping the basic functions in classes.
In order to conduct a request, the programmer instantiates those classes and provides the arguments using member functions of the 
classes. Temboo pulls information that you give it through your account, such as the API access key, so you do not have to enter it
more than once.