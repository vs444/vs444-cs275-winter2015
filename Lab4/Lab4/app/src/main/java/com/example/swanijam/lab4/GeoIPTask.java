package com.example.swanijam.lab4;

import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import com.google.gson.*;
import java.net.*;
import java.io.InputStreamReader;
import java.io.InputStream;

import android.util.Log;
import android.widget.*;
import android.app.Activity;

public class GeoIPTask extends AsyncTask<Void, Void, ArrayList<String>>{
    ArrayList<String> results = new ArrayList<String>();
    TextView text;
    ImageView img;
    MainActivity MainAct;
    ListView list;
    public GeoIPTask(ImageView img, ListView list, TextView text, MainActivity parent){
      this.img = img;
      this.list = list;
      this.text = text;
        MainAct = parent;

    }
    protected void onProgressUpdate() {

    }

    protected void onPostExecute(ArrayList<String> results) {
        //apply the results to the different views
        text.setText(results.get(0));
        //use the imageDownloadertask to load the image
        new ImageDownloader(img).execute(results.get(1));
        String[] adapterInputs = new String[results.size()-3];
        adapterInputs = results.subList(2, results.size()-1).toArray(adapterInputs);
        ArrayAdapter<String> itemsAdapter = new ArrayAdapter(MainAct, android.R.layout.simple_list_item_1, android.R.id.text1, adapterInputs);
        list.setAdapter(itemsAdapter);

      //access the Main Activity's SQLite database and save the new entries
        SQLiteDatabase db = MainAct.db;
        db.execSQL("INSERT INTO data VALUES('" + 1 + "','" + results.get(0) + "');");
        db.execSQL("INSERT INTO data VALUES('" + 2 + "','" + results.get(1) + "');");
        for (int i = 2; i < results.size(); i++) {
            db.execSQL("INSERT INTO data VALUES('" + (i + 1) + "','" + results.get(i) + "');");
        }
    }

    @Override
    protected ArrayList<String> doInBackground(Void... params){

        try {

            //my API KEY
            //a4cd4bd7aef67e1d

            //connect to the webpage to get the information
            String autoAddressURL = "http://api.wunderground.com/api/" + "e1e42d770c25cf8c" + "/geolookup/q/autoip.json";
            URL url = new URL(autoAddressURL);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();

            // Convert to a JSON object
            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonObject rootobj = root.getAsJsonObject();

            // Get some data elements
            JsonObject location = rootobj.get("location").getAsJsonObject();

            String city = location.get("city").getAsString();
            String zip = location.get("zip").getAsString();
            String state = location.get("state").getAsString();
            String lat = location.get("lat").getAsString();
            String lon = location.get("lon").getAsString();

            //print out the first line of output
            //Ex: "Hourly forecast for Philadelphia, PA, 19019; 40.11690903, -75.01428223:"

            results.add("Hourly forecast for " + city + ", " + state + ", " + zip + "; " + lat + ", " + lon + ":");
//            System.out.println("Hourly forecast for " + city + ", " + state + ", " + zip + "; " + lat + ", " + lon + ":");



            //connect to the webpage to get the hourly forecast
            String hourlyForecastURL = "http://api.wunderground.com/api/" + "e1e42d770c25cf8c" + "/hourly/q/" + state + "/" + city + ".json";
            URL hurl = new URL(hourlyForecastURL);
            request = (HttpURLConnection) hurl.openConnection();
            request.connect();

            // Convert to a JSON object
            jp = new JsonParser();
            root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            rootobj = root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive

            // convert to json array of hourly data
            JsonArray hourly = rootobj.get("hourly_forecast").getAsJsonArray();
            JsonObject hour;
            //loop through each our and print the important information
            for (int i = 0; i < hourly.size(); i++) {
                hour = hourly.get(i).getAsJsonObject();
                if(i == 0){
                    results.add(hour.get("icon_url").getAsString());
                }
                String timedate = hour.get("FCTTIME").getAsJsonObject().get("pretty").getAsString();
                String conditions = hour.get("condition").getAsString();
                String temperature = hour.get("temp").getAsJsonObject().get("english").getAsString() + "F";
                String humidity = hour.get("humidity").getAsString();
                results.add(timedate + ": " + conditions + ". " + temperature + ". Humidity: " + humidity);
            }

        } catch(Exception e){

        }

        return results;
    }
}
