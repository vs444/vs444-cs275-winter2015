package com.example.swanijam.lab4;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import android.content.Context;

import java.util.ArrayList;
import java.util.Date;


public class MainActivity extends ActionBarActivity {
    public SQLiteDatabase db;
    Time today;

    TextView text ;
    ListView list ;
    ImageView image ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (TextView) findViewById(R.id.textView);
        list = (ListView) findViewById(R.id.listView);
        image = (ImageView) findViewById(R.id.imageView);

        db=openOrCreateDatabase("WeatherHoursDB5", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS data(int VARCHAR,data VARCHAR);");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    public void refresh(View view){
        today = new Time();
        today.setToNow();
        boolean needsUpdate = true;
        text.setText("Checking Cache...");
        //get the time first so we can check if we need an update
        Cursor c=db.rawQuery("SELECT * FROM data WHERE int='"+0+"'", null);
        int seconds = today.second + 3600;
        if(c.getCount() != 0 && c.moveToFirst())
        {
            text.setText("Found Previous Run Time...");
            seconds = Integer.parseInt(c.getString(1));
        }

        //use the time to determine if we need an update
        if(today.second - seconds < 3600) {
            c = db.rawQuery("SELECT * FROM data", null);
            text.setText("Determining if Old Data is Available...");

            if(c.getCount()==0)
            {
                //if there is no old data we need an update
                needsUpdate = true;
            }else {
                needsUpdate = false;
                text.setText("Reading From Cache...");
                //Use old data if there is any
                ArrayList<String> results = new ArrayList<String>();
                while (c.moveToNext()) {
                    results.add(c.getString(1));
                }
                text.setText(results.get(1));
                new ImageDownloader(image).execute(results.get(2));
                String[] adapterInputs = new String[results.size()-3];
                adapterInputs = (results.subList(3, results.size())).toArray(adapterInputs);
                ArrayAdapter<String> itemsAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, android.R.id.text1, adapterInputs);
                list.setAdapter(itemsAdapter);
            }
        }
        if(needsUpdate){
            needsUpdate = false;
            this.deleteDatabase("WeatherHoursDB5");
            db=openOrCreateDatabase("WeatherHoursDB5", Context.MODE_PRIVATE, null);
            db.execSQL("CREATE TABLE IF NOT EXISTS data(int VARCHAR,data VARCHAR);");

            //get and display new data using GeoIPTask
            text.setText("Updating...");
            GeoIPTask task = new GeoIPTask(image, list, text, this);
            task.execute();
            db.execSQL("INSERT INTO data VALUES('" + 0 + "','" + today.second + "');");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
