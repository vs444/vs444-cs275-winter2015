Partcipants: Vishnu Sreenivasan (vs444), James Swanick (jes432)

This project uses a basic scrolling list in android to display hourly forecast information
for the IP of the device. The program implements geoIPlookup from weather underground to
find the device's lat and long. It then puts that information into the hourly forecast to
get the information for the forecast. We used ImageView to take the image from the link returned
by the JSON and display it on the screen. SQLite database is used to store information. The
forecast info is pulled from this SQLite database only if it has been more than 1 hour since the 
last network call.

There are no specific instructions for testing the program. Just run it.