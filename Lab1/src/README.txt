Name: Vishnu Sreenivasan
ID: vs444

My design contains only a main method. There are 2 HTTP connections. One is to the Geolookup API and
the other is to the hourly forecast API. From the Geolookup API I pull information about the user's
current location such as the city, state, and zip code. This information is used later when I need
to find the hourly forecast for that specific location. My developer key is hard coded.