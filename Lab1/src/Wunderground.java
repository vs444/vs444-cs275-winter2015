import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import com.google.gson.JsonElement;
// Requires gson jars
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Wunderground {         
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		String sURL = "http://api.wunderground.com/api/3742a6902ba9a9a4/geolookup/q/autoip.json";
		
		// Connect to the URL
		URL url = new URL(sURL);
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.connect();
		
		// Convert to a JSON object to print data
    	JsonParser jp = new JsonParser();
    	JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
    	JsonObject rootobj = root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive
    	
    	// Get some data elements and print them
    	String zipCode = rootobj.get("location").getAsJsonObject().get("zip").getAsString();
		//Save geolookup info for later query
		String state = rootobj.get("location").getAsJsonObject().get("state").getAsString();
		String city = rootobj.get("location").getAsJsonObject().get("city").getAsString();
		
		System.out.println("The location is "+city+", "+state+" "+zipCode);

		//change city name to right format
		
		city.replace(" ", "_");
		
		String sURL2 = "http://api.wunderground.com/api/3742a6902ba9a9a4/hourly/q/"+state+"/"+city+".json";
		URL url2 = new URL(sURL2);
		HttpURLConnection request2 = (HttpURLConnection) url2.openConnection();
		request2.connect();
		
		JsonParser jp2 = new JsonParser();
    	JsonElement root2 = jp2.parse(new InputStreamReader((InputStream) request2.getContent()));
    	JsonObject rootobj2 = root2.getAsJsonObject();
    	//object to iterate through JsonArray
  
    	String time = "";
    	String condition = "";
    	String temperature = "";
    	String relativeHumidity = "";
    
    	for(int a = 0; a<36; a++)
    	{
    		temperature = rootobj2.get("hourly_forecast").getAsJsonArray().get(a).getAsJsonObject().get("temp").getAsJsonObject().get("english").getAsString();
    		condition = rootobj2.get("hourly_forecast").getAsJsonArray().get(a).getAsJsonObject().get("condition").getAsString();
    		relativeHumidity = rootobj2.get("hourly_forecast").getAsJsonArray().get(a).getAsJsonObject().get("humidity").getAsString();
    		time = rootobj2.get("hourly_forecast").getAsJsonArray().get(a).getAsJsonObject().get("FCTTIME").getAsJsonObject().get("pretty").getAsString();
    		
    		System.out.println(time+", "+condition+", "+temperature+" degrees, "+relativeHumidity+"% humidity");
    	}
	}
}