package com.example.swanijam.tictactoe;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;
import android.content.Context;


public class MainActivity extends Activity {
    String playerSymbol = "X";
    Button[] buttons = new Button[10];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //initialize a list of button references
        buttons[0] = null;
        buttons[1] = ((Button) findViewById(R.id.button));
        buttons[2] = ((Button) findViewById(R.id.button2));
        buttons[3] = ((Button) findViewById(R.id.button3));
        buttons[4] = ((Button) findViewById(R.id.button4));
        buttons[5] = ((Button) findViewById(R.id.button5));
        buttons[6] = ((Button) findViewById(R.id.button6));
        buttons[7] = ((Button) findViewById(R.id.button7));
        buttons[8] = ((Button) findViewById(R.id.button8));
        buttons[9] = ((Button) findViewById(R.id.button9));
    }


    //called after each turn to switch the player
    public void switchPlayers(){
        if(playerSymbol.equals("X")){
            playerSymbol = "O";
        } else {
            playerSymbol = "X";
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //******************************************************************************
    //nine implementatinos of an onClick which changes the text on each button when clicked
    //(i know, nine nearly identical methods.)
    //*****************************************************************************

    public void button1click(View view){
        if(buttons[1].getText().equals("")) {
            buttons[1].setText(playerSymbol);
            switchPlayers();
            checkGameStatus();
        }
    }
    public void button2click(View view){
        if(buttons[2].getText().equals("")) {
            buttons[2].setText(playerSymbol);
            switchPlayers();
            checkGameStatus();
        }

    }
    public void button3click(View view){
        if(buttons[3].getText().equals("")) {
            buttons[3].setText(playerSymbol);
            switchPlayers();
            checkGameStatus();
        }
    }

    public void button4click(View view){
        if(buttons[4].getText().equals("")) {
            buttons[4].setText(playerSymbol);
            switchPlayers();
            checkGameStatus();
        }
    }

    public void button5click(View view){
        if(buttons[5].getText().equals("")) {
            buttons[5].setText(playerSymbol);
            switchPlayers();
            checkGameStatus();
        }
    }

    public void button6click(View view){
        if(buttons[6].getText().equals("")){
            buttons[6].setText(playerSymbol);
            switchPlayers();
            checkGameStatus();
        }
    }

    public void button7click(View view) {
        if (buttons[7].getText().equals("")){
            buttons[7].setText(playerSymbol);
            switchPlayers();
            checkGameStatus();
        }
    }

    public void button8click(View view) {
        if (buttons[8].getText().equals("")){
            buttons[8].setText(playerSymbol);
            switchPlayers();
            checkGameStatus();
        }
    }
    public void button9click(View view){
        if(buttons[9].getText().equals("")) {
            buttons[9].setText(playerSymbol);
            switchPlayers();
            checkGameStatus();
        }
    }


    //checks all 8 possibilities of a tic-tac-toe win, as well as the combination of a tie, and prompts the end-game message
    public void checkGameStatus(){
        String[] players = {"X", "O"};

        for(String player : players){
            if(buttons[1].getText().equals(player) && buttons[2].getText().equals(player) && buttons[3].getText().equals(player)){
                winMessage(player);
            } else if(buttons[1].getText().equals(player) && buttons[4].getText().equals(player) && buttons[7].getText().equals(player)){
                winMessage(player);
            } else if(buttons[1].getText().equals(player) && buttons[5].getText().equals(player) && buttons[9].getText().equals(player)){
                winMessage(player);
            } else if(buttons[2].getText().equals(player) && buttons[5].getText().equals(player) && buttons[8].getText().equals(player)){
                winMessage(player);
            } else if(buttons[3].getText().equals(player) && buttons[5].getText().equals(player) && buttons[7].getText().equals(player)){
                winMessage(player);
            } else if(buttons[3].getText().equals(player) && buttons[6].getText().equals(player) && buttons[9].getText().equals(player)){
                winMessage(player);
            } else if(buttons[4].getText().equals(player) && buttons[5].getText().equals(player) && buttons[6].getText().equals(player)){
                winMessage(player);
            } else if(buttons[7].getText().equals(player) && buttons[8].getText().equals(player) && buttons[9].getText().equals(player)){
                winMessage(player);
            } else if(!(buttons[1].getText().equals("")) &&
                    !(buttons[2].getText().equals("")) &&
                    !(buttons[3].getText().equals("")) &&
                    !(buttons[4].getText().equals("")) &&
                    !(buttons[5].getText().equals("")) &&
                    !(buttons[6].getText().equals("")) &&
                    !(buttons[7].getText().equals("")) &&
                    !(buttons[8].getText().equals("")) &&
                    !(buttons[9].getText().equals(""))){
                winMessage("Nobody");
            }
        }
    }


    //displays a Toast about the winner ( or lack thereof)
    //and resets the game
    public void winMessage(String player){
        Context context = getApplicationContext();
        CharSequence text = player + " wins!!!";
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

        for(int i = 1; i < 10; i++){
            buttons[i].setText("");
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
