package com.example.swanijam.finalproj;

import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by swanijam on 3/16/15.
 */
public class CelebDatabase {
    String[][] data = {};
    public CelebDatabase(InputStream fileContentStream){
        data = readCSV(fileContentStream);
    }

    public String[][] readCSV(InputStream in)
    {;
        BufferedReader br;
        String line = "";
        String cvsSplitBy = ",";
        String[] information;
        ArrayList<String[]> data = new ArrayList<String[]>();
        try {
            br = new BufferedReader(new InputStreamReader(in));
            while ((line = br.readLine()) != null) {
                // use comma as separator
                Log.v("TAUNT", line);
                information = line.split(cvsSplitBy);
                data.add(information);
            }
            br.close();

        } catch (FileNotFoundException e) {

            Log.v("TAUNT", "tauntFNF");
        } catch (IOException e) {

            Log.v("TAUNT", "tauntyIO");
        }
        String[][] array = new String[data.size()][];
        for (int i = 0; i < data.size(); i++) {
            String[] row = data.get(i);
            array[i] = row;
        }
        return array;
    }

    public boolean personFound(String personName){
        for (String[] item : data)
            if(item[0].equals(personName))
                return true;

        return false;
    }

    public String[] getIDs(String personName){
        for (String[] item : data)
            if(item[0].equals(personName)) {
                Log.v("TAUNTY", item[1] + " " + item[2] + " " + item[3]);
                return item;
            }

        return null;
    }

}
