package com.example.swanijam.finalproj;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class webActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        WebView webV = (WebView) findViewById(R.id.webView);
        webV.loadUrl((String) getIntent().getExtras().get("URL"));

        webV.setWebViewClient(new WebViewClient() {
            int counter = 0;
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if(counter == 1) {

                    Log.v("TAUNTY", "YUP " + url);
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("st", (String)getIntent().getExtras().get("st"));
                    returnIntent.putExtra("userID", (String)getIntent().getExtras().get("userID"));
                    setResult(1, returnIntent);
                    finish();
                    return false;
                } else {
                    Log.v("TAUNTY", "NOPE " + url);
                    counter++;
                    return false;
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_web, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
