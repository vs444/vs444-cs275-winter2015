package com.example.swanijam.finalproj;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

class instaAdapter extends BaseAdapter {

    Context context;
    ArrayList<ArrayList<String>> data;
    private static LayoutInflater inflater = null;

    public instaAdapter(Context context, ArrayList<ArrayList<String>> data) {
        Log.v("TAUNTY", "instaAdapter constructor");
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // TODO Auto-generated method stub
        View vi = convertView;

        if (vi == null)
            vi = inflater.inflate(R.layout.insta_row, null);
        TextView caption = (TextView) vi.findViewById(R.id.caption);
        TextView timestamp = (TextView) vi.findViewById(R.id.timeStamp);
        TextView service = (TextView) vi.findViewById(R.id.serviceName);
        ImageView image = (ImageView) vi.findViewById(R.id.imageView);
        image.setAlpha((float) 0.0);
        ImageDownloader imgDL = new ImageDownloader(image);
        imgDL.execute(data.get(position).get(1));

        caption.setText(data.get(position).get(0));
        timestamp.setText(data.get(position).get(3));
        service.setText(data.get(position).get(4));

        return vi;
    }
}