package com.example.swanijam.finalproj;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.webkit.WebView;

import com.temboo.Library.Facebook.OAuth.InitializeOAuth;
import com.temboo.core.TembooSession;

/**
 * Created by swanijam on 3/15/15.
 */
public class initOAuthFacebook extends AsyncTask<Void, Void, String>{
    MainActivity mainAct;
    String stateT;
    TembooSession session;
    String userID;
    public initOAuthFacebook(MainActivity mainAct, TembooSession session, String stateToken, String user){
        this.session = session;
        this.stateT = stateToken;
        this.mainAct = mainAct;
        userID = user;
    }
    protected void onPostExecute(String url) {
        Log.v("TAUNTY", "Execute");
        authorize(url);
    }

    public void authorize(String authorizationURL) {
        System.out.println("LAKJDLKJHSLKJAHS");
        WebView webView = new WebView(mainAct);
        Intent showAuthorization = new Intent(mainAct, webActivity.class);
        showAuthorization.putExtra("URL", authorizationURL);
        showAuthorization.putExtra("st", stateT);
        showAuthorization.putExtra("userID", userID);
        mainAct.startActivityForResult(showAuthorization, 2);

    }
    @Override
    protected String doInBackground(Void... params) {
        try {
            InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

            // Get an InputSet object for the choreo
            InitializeOAuth.InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

            // Set inputs
            initializeOAuthInputs.set_CustomCallbackID(stateT);
            initializeOAuthInputs.set_AppID("1422611944706414");

            // Execute Choreo
            InitializeOAuth.InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
            Log.v("TAUNTY", initializeOAuthResults.get_CallbackID());
            return initializeOAuthResults.get_AuthorizationURL();

        } catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }
}
