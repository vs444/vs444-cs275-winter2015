package com.example.swanijam.finalproj;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.temboo.Library.Instagram.GetRecentMediaForUser;
import com.temboo.Library.Instagram.OAuth.FinalizeOAuth;
import com.temboo.Library.Instagram.OAuth.InitializeOAuth;
import com.temboo.core.TembooSession;

import org.json.JSONArray;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class InstagramTask extends AsyncTask<Void, Void, ArrayList<ArrayList<String>> >{
    MainActivity mainAct = null;
    TembooSession session;
    String userID;
    String stateT;
    public InstagramTask(MainActivity main, TembooSession session, String stateToken, String user){
        mainAct = main;
        stateT = stateToken;
        this.session = session;
        this.userID = user;
    }

    protected void onPostExecute(ArrayList<ArrayList<String>> results) {


        mainAct.provideResults(results);
    }
    @Override
    protected ArrayList<ArrayList<String>> doInBackground(Void... params) {

        ArrayList<ArrayList<String>> returnValues = new ArrayList<ArrayList<String>>();

        try {

            // Instantiate the Choreo, using a previously instantiated TembooSession object, eg:



            FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

            // Get an InputSet object for the choreo
            FinalizeOAuth.FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

            // Set inputs
            finalizeOAuthInputs.set_CallbackID("vs444/" + stateT);
            finalizeOAuthInputs.set_ClientSecret("282e6979490641848acf292c82957098");
            finalizeOAuthInputs.set_ClientID("6aac261785c04005b50d3fc82a244826");

            // Execute Choreo
            FinalizeOAuth.FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);


            GetRecentMediaForUser getRecentMediaForUserChoreo = new GetRecentMediaForUser(session);

            // Get an InputSet object for the choreo
            GetRecentMediaForUser.GetRecentMediaForUserInputSet getRecentMediaForUserInputs = getRecentMediaForUserChoreo.newInputSet();

            // Set inputs
            getRecentMediaForUserInputs.set_AccessToken(finalizeOAuthResults.get_AccessToken());
            getRecentMediaForUserInputs.set_UserID(userID);
            Log.v("TAUNTY", userID+"!!");


            // Execute Choreo
            GetRecentMediaForUser.GetRecentMediaForUserResultSet getRecentMediaForUserResults = getRecentMediaForUserChoreo.execute(getRecentMediaForUserInputs);

            JSONObject userData = new JSONObject(getRecentMediaForUserResults.get_Response());

            JSONArray data = userData.getJSONArray("data");

            for(int i = 0; i < data.length(); i++){
                ArrayList<String> attributes = new ArrayList<String>();
                JSONObject posts = data.getJSONObject(i);
                String caption;

                Log.v("TAUNTY", i + " " + posts.getJSONObject("caption").getString("text"));
                if(posts.getJSONObject("caption").getString("text") == null)
                {
                    caption = "";
                }
                else
                {
                    caption = posts.getJSONObject("caption").getString("text");
                }
                String imageURL = posts.getJSONObject("images").getJSONObject("standard_resolution").getString("url");
                String postURL = posts.getString("link");
                String createdTime = posts.getString("created_time");
                attributes.add(caption);
                attributes.add(imageURL);
                attributes.add(postURL);
                long foo = Long.parseLong(createdTime)*1000;
                Date date = new Date(foo);
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
                attributes.add(formatter.format(date));
                attributes.add("Instagram");
                returnValues.add(attributes);

            }
            //returnValues index 0 = caption, index 1 = imageURL, index 2 = postURL, index 3 = createdTime
            Log.v("TAUNTY", "memes 26.0");
        } catch (Exception e){
            Log.v("TAUNTY", "taunty");
            e.printStackTrace();
        }
        return returnValues;
    }


}
