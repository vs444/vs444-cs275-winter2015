package com.example.swanijam.finalproj;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by swanijam on 3/16/15.
 */
public class PinsList {
    ArrayList<String> list;
    Context cont;

    public PinsList(ArrayList<String> pins){
        list = pins;
    }

    public void addPin(String name){
        list.add(name);
    }
    public ArrayList<String> getList(){
        return list;
    }

    public boolean hasPin(String person){
        for(String pin: list){
            if(pin.equals(person)){
                return true;
            }
         }

        return false;
    }
    public void savePins(){

    }
}
