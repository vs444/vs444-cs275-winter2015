package com.example.swanijam.finalproj;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Facebook.OAuth.FinalizeOAuth;
import com.temboo.Library.Facebook.Reading.ProfileFeed;
import com.temboo.core.TembooSession;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class FacebookTask extends AsyncTask<Void, Void, ArrayList<ArrayList<String>>> {
    MainActivity mainAct = null;
    TembooSession session;
    String stateT;
    String userID;
    public FacebookTask(MainActivity main, TembooSession session, String stateToken, String user) {
        mainAct = main;
        stateT = stateToken;
        this.session = session;
        userID = user;
    }

    protected void onPostExecute(ArrayList<ArrayList<String>> results) {
        Log.v("TAUNTY", results.get(0).get(0));
        mainAct.provideResults(results);
    }

    @Override
    protected ArrayList<ArrayList<String>> doInBackground(Void... params) {

        Log.v("TAUNTY", "doInBackground");
        ArrayList<ArrayList<String>> returnValues = new ArrayList<ArrayList<String>>();

        try {

            // Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
            FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

            // Get an InputSet object for the choreo
            FinalizeOAuth.FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

            // Set inputs
            finalizeOAuthInputs.set_CallbackID("vs444/" + stateT);
            finalizeOAuthInputs.set_AppSecret("f1c5f6cd6a9352906f9e3b4c870592d5");
            finalizeOAuthInputs.set_AppID("1422611944706414");

            // Execute Choreo
            FinalizeOAuth.FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);

            ProfileFeed profileFeedChoreo = new ProfileFeed(session);

            // Get an InputSet object for the choreo
            ProfileFeed.ProfileFeedInputSet profileFeedInputs = profileFeedChoreo.newInputSet();

            // Set inputs
            profileFeedInputs.set_AccessToken(finalizeOAuthResults.get_AccessToken());
            profileFeedInputs.set_ProfileID(userID);
            // Execute Choreo
            ProfileFeed.ProfileFeedResultSet profileFeedResults = profileFeedChoreo.execute(profileFeedInputs);

            JsonParser parser = new JsonParser();
            JsonObject userData = (JsonObject) parser.parse(profileFeedResults.get_Response());

            JsonArray data = userData.get("data").getAsJsonArray();
            Log.v("TAUNTY", "beginLoop");
            for (int i = 0; i < data.size(); i++) {
                ArrayList<String> attributes = new ArrayList<String>();
                JsonObject posts = data.get(i).getAsJsonObject();
                String message;
                String time;
                String postURL;
                postURL = posts.get("actions").getAsJsonArray().get(0).getAsJsonObject().get("link").getAsString();
                try {
                    message = posts.get("message").getAsString();
                } catch (Exception e) {
                    message = "";
                }
                time = posts.get("updated_time").getAsString();

                DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ENGLISH);
                DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
                Date date = null;

                date = originalFormat.parse(time);

                String formattedDate = targetFormat.format(date);

                attributes.add(message);
                attributes.add("");
                attributes.add(postURL);
                attributes.add(formattedDate);
                attributes.add("Facebook");
                returnValues.add(attributes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnValues;

    }
}

