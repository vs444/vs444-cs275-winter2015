package com.example.swanijam.finalproj;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.core.TembooSession;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by swanijam on 3/15/15.
 */
public class TwitterTask extends AsyncTask<Void, Void, ArrayList<ArrayList<String>>> {

    private final MainActivity mainAct;
    private final TembooSession session;
    String userID;

    public TwitterTask(MainActivity main, TembooSession sesh, String user){
        this.mainAct = main;
        this.session = sesh;
        userID = user;
    }

    @Override
    protected void onPostExecute(ArrayList<ArrayList<String>> arrayLists) {

        Log.v("TAUNTY", "onPostExecute()");
        mainAct.provideResults(arrayLists);
    }

    @Override
    protected ArrayList<ArrayList<String>> doInBackground(Void... params) {
        ArrayList<ArrayList<String>> returnValues = new ArrayList<ArrayList<String>>();
        try {
            Log.v("TAUNTY", "doInBackground()");
            // Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
            UserTimeline userTimelineChoreo = new UserTimeline(session);

            // Get an InputSet object for the choreo
            UserTimeline.UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();

            // Set inputs
            userTimelineInputs.set_ScreenName(userID);
            userTimelineInputs.set_AccessToken("2941048179-MUuF3XqTH2r6aATptXqQiehJuheuDdwTQ9tDMfG");
            userTimelineInputs.set_AccessTokenSecret("JO0xDJi4k1hLwbqNqvi9LTBBmS7U3JJnKcbTQgHdgFv3c");
            userTimelineInputs.set_ConsumerSecret("w1vSUwa7wfEdnpOLpy1L1zYkCDfM6q4Kv1n4y2Kt50NoITU4i3");
            userTimelineInputs.set_ConsumerKey("T83vMkApHW6J9l6W2IduQPHTv");

            // Execute Choreo
            UserTimeline.UserTimelineResultSet userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);
            JsonParser parser = new JsonParser();
            JsonArray userTimeLine = (JsonArray) parser.parse(userTimelineResults.get_Response());
            for(int i = 0; i < userTimeLine.size(); i++){
                ArrayList<String> attributes = new ArrayList<String>();

                JsonObject tweets = userTimeLine.get(i).getAsJsonObject();
                String tweetText = tweets.get("text").getAsString();
                String createdTime = tweets.get("created_at").getAsString();
                String tweetURL = "https://twitter.com/"+userID+"/status/"+tweets.get("id");

                Log.v("TAUNTY", i + " " + tweetText);

                //convert given time to standard format for app
                DateFormat originalFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH);
                DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
                Date date = null;
                try {
                    date = originalFormat.parse(createdTime);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                String formattedDate = targetFormat.format(date);

                attributes.add(tweetText);
                attributes.add("");
                attributes.add(tweetURL);
                attributes.add(formattedDate);
                attributes.add("Twitter");
                returnValues.add(attributes);
            }

        } catch(Exception e){
            e.printStackTrace();
        }
        return returnValues;
    }
}
