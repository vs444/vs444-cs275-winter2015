package com.example.swanijam.finalproj;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.*;

import com.android.volley.Response;
import com.cloudmine.api.CMApiCredentials;
import com.cloudmine.api.CMFile;
import com.cloudmine.api.CacheableCMFile;
import com.cloudmine.api.rest.response.FileLoadResponse;
import com.temboo.core.TembooSession;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.SecureRandom;
import java.util.ArrayList;

public class MainActivity extends Activity{
    // Find this in your developer console
    private static final String APP_ID = "e4a32e04ebe2486daca56c64e5fdd416";
    // Find this in your developer console
    private static final String API_KEY = "fc5205e88af043a98cf410e4bb6ff36c";

    TembooSession sesh = null;
    SearchView search;
    Spinner spin;
    CelebDatabase db;
    PinsList pl;
    public SQLiteDatabase pinsDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // This will initialize your credentials
        CMApiCredentials.initialize(APP_ID, API_KEY, getApplicationContext());


        CacheableCMFile.loadFile(this, "1c5143aa6d6d8c43e6ba6da1b34b86de", new Response.Listener<FileLoadResponse>() {
            @Override
            public void onResponse(FileLoadResponse fileLoadResponse) {
                CMFile file = fileLoadResponse.getFile();
                db = new CelebDatabase( file.getFileContentStream() );
            }
        });


        pinsDB = openOrCreateDatabase("Pins", Context.MODE_PRIVATE, null);
        pinsDB.execSQL("CREATE TABLE IF NOT EXISTS data(pin VARCHAR);");
        pl = new PinsList(readPins());


        spin = (Spinner) findViewById(R.id.spinner);
        String[] adapterInputs = {"select feed", "All", "Facebook", "Twitter", "Instagram"};

        ArrayAdapter<String> itemsAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, android.R.id.text1, adapterInputs);
        spin.setAdapter(itemsAdapter);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                loadPosts(search.getQuery().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        search = (SearchView) findViewById(R.id.searchView);
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                loadPosts(spin.getSelectedItem().toString());
                search.clearFocus();
                if(!db.personFound(query)){
                    Context context = getApplicationContext();
                    CharSequence text = "Person Not in Database";
                    int duration = Toast.LENGTH_LONG;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                } else {
                    loadPosts(query);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }

    private void getInstagram(String query) {
        SecureRandom random = new SecureRandom();
        String stateToken = "instagram-" + random.nextInt();
        try {
            Log.v("TAUNTY", "getInstagram()");
            sesh = new TembooSession("vs444", "myFirstApp", "67abfb694af14356a70f7a81861ab6c4");
            Log.v("TAUNTY", db.getIDs(query)[1]+"---------------");
            InitOauthInsta init = new InitOauthInsta(this, sesh, stateToken, db.getIDs(query)[1]);
            init.execute();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    private void getTwitter(String query) {
        try {
            Log.v("TAUNTY", "getTwitter() "+query);
            sesh = new TembooSession("vs444", "myFirstApp", "67abfb694af14356a70f7a81861ab6c4");
            TwitterTask tweety = new TwitterTask(this, sesh, db.getIDs(query)[2]);
            tweety.execute();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    private void getFacebook(String query) {
        try {
            SecureRandom random = new SecureRandom();
            String stateToken = "facebook-" + random.nextInt();
            Log.v("TAUNTY", "getTwitter()");
            sesh = new TembooSession("vs444", "myFirstApp", "67abfb694af14356a70f7a81861ab6c4");
            initOAuthFacebook face = new initOAuthFacebook(this, sesh, stateToken,  db.getIDs(query)[3]);
            face.execute();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    ArrayList<ArrayList<String>> aggregate;
    int aggregateAdditions;
    private void getAll(String query) {
        aggregate = new ArrayList<>();
        aggregateAdditions = 0;
        getFacebook(query);
        getInstagram(query);
        getTwitter(query);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadPosts(String query) {
        ListView list = (ListView) findViewById(R.id.listPosts);

        Log.v("TAUNTY", query + " selected");


        colorizeView();

        String feed = spin.getSelectedItem().toString();
        if (feed.equals("All")) {
            //load listview with ALL posts
            try {
                getAll(query);
            }catch(Exception e){
                e.printStackTrace();
            }

        } else if (feed.equals("Facebook")) {
           try {
               getFacebook(query);
           }catch(Exception e){
               e.printStackTrace();
           }
            } else if (feed.equals("Twitter")) {
            //load listview with Twitter posts
           try {
               getTwitter(query);
           }catch(Exception e){
               e.printStackTrace();
           }

        } else if (feed.equals("Instagram")) {
            //load listview with Instagram posts
            try {
                getInstagram(query);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    public void colorizeView(){
        int color = 0xFFFFFFFF;
        int buttonColor = 0xFFAAAAAA;
        String theme = spin.getSelectedItem().toString();
        if (theme.equals("All")) {
            color = 0xFF95f486;
            buttonColor = 0xFF5EB759;

        } else if (theme.equals("Facebook")) {
            color = 0xFFAFBDD4;
            buttonColor = 0xFF3B5998;
        } else if (theme.equals("Twitter")) {
            color = 0xFFC0DEED;
            buttonColor = 0xFF00ACED;
        } else if (theme.equals("Instagram")) {
            color = 0xFFFFFFFF;
            buttonColor = 0xFF3B5998;
        }
        Button pins = (Button) findViewById(R.id.pins);
        LinearLayout lin = (LinearLayout) findViewById(R.id.lin);
        ListView list = (ListView) findViewById(R.id.listPosts);
        Button pin = (Button) findViewById(R.id.pin);
        LinearLayout linlayout = (LinearLayout) findViewById(R.id.linLayout);

        spin.setBackgroundColor(color);
        list.setBackgroundColor(color);
        lin.setBackgroundColor(color);
        linlayout.setBackgroundColor(color);
        pin.setBackgroundColor(buttonColor);
        pins.setBackgroundColor(buttonColor);

        spin.invalidate();
        list.invalidate();
        lin.invalidate();
        pin.invalidate();
        pins.invalidate();

    }
    public void addPin(View v) {
        SearchView search = (SearchView) findViewById(R.id.searchView);
        String currentSearch = search.getQuery().toString();
        if(!currentSearch.equals(""))
            if(db.personFound(currentSearch) && !pl.hasPin(currentSearch)) {
                Log.v("TAUNTY", currentSearch +"");
                pl.addPin(currentSearch);
                this.deleteDatabase("Pins");
                pinsDB = openOrCreateDatabase("Pins", Context.MODE_PRIVATE, null);
                pinsDB.execSQL("CREATE TABLE IF NOT EXISTS data(pin VARCHAR);");
                for(String pin : pl.getList()){
                    pinsDB.execSQL("INSERT INTO data VALUES('"+ pin+"');");

                    Context context = getApplicationContext();
                    CharSequence text = "Pinned! :)";
                    int duration = Toast.LENGTH_LONG;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }

        //access CSV and add to it

    }

    public void showPins(View v) {
        Intent pins = new Intent(MainActivity.this, ShowPinsActivity.class);
        pins.putExtra("pins", pl.getList());
        startActivityForResult(pins, 3);
    }

    //called by AsyncTasks to return requested feeds
    public void provideResults(ArrayList<ArrayList<String>> results) {
        if(spin.getSelectedItem().toString().equals("All")) {
            Log.v("TAUNTY", "added");
           aggregate.addAll(results);
           aggregateAdditions++;

           search.clearFocus();
           if(aggregateAdditions == 3){
               ListView lv = (ListView) findViewById(R.id.listPosts);
               ArrayList<ArrayList<String>> sorted = new ArrayList<ArrayList<String>>();
               sortArrayList(sorted, aggregate);
               lv.setAdapter(new instaAdapter(this, sorted));
               lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                   @Override
                   public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                       String url = ((ArrayList<String>) (parent).getAdapter().getItem(position)).get(2);
                       Uri uriUrl = Uri.parse(url);
                       Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                       startActivity(launchBrowser);
                       return false;
                   }
               });
           }
        } else {
            ListView lv = (ListView) findViewById(R.id.listPosts);
            lv.setAdapter(new instaAdapter(this, results));
            lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    String url = ((ArrayList<String>) (parent).getAdapter().getItem(position)).get(2);
                    Uri uriUrl = Uri.parse(url);
                    Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                    startActivity(launchBrowser);
                    return false;
                }
            });
            search.clearFocus();
        }
    }

    //this is called when a webView closes. used to prompt the OAuth protocols to continue.
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            Log.v("TAUNTY", data.getExtras().get("userID")+"");
            InstagramTask instaTask = new InstagramTask(this, sesh, ((String) data.getExtras().get("st")), ((String) data.getExtras().get("userID")));
            instaTask.execute();
        } else if(requestCode == 2){
            FacebookTask face = new FacebookTask(this, sesh,((String) data.getExtras().get("st")), ((String) data.getExtras().get("userID")));
            face.execute();
        } else if(requestCode == 3){
            if(!((String)data.getExtras().get("pin")).equals("None"))
                search.setQuery(((String)data.getExtras().get("pin")) , true);
                search.setIconified(false);
                search.clearFocus();
        }
    }

    //takes a temperory arraylist, as well as the original arraylist. The temporary arraylist will contain the sorted elements.
    public ArrayList<ArrayList<String>> sortArrayList (ArrayList<ArrayList<String>> finalList, ArrayList<ArrayList<String>> list)
    {
        String max = "0000-00-00 00:00:00 EDT";
        int maxIndex = 0;
        if(list.size()==0)
        {
            return list;
        }
        for(int i = 0; i < list.size(); i++)
        {
            if(list.get(i).get(3).compareTo(max) > 0)
            {
                max = list.get(i).get(3);
                maxIndex = i;
            }

        }
        finalList.add(list.get(maxIndex));
        Log.v("TAUNTY", list.get(maxIndex).get(3));
        list.remove(maxIndex);
        return sortArrayList(finalList, list);
    }

    public ArrayList<String> readPins()
    {
        ArrayList<String> data = new ArrayList<String>();
       Cursor c = pinsDB.rawQuery("SELECT * FROM data", null);
        if(c.getCount() == 0){
            Log.v("TAUNTY", "no pins loaded");
        } else {
            Log.v("TAUNTY", "loaded pins");
            while(c.moveToNext()){
                data.add(c.getString(0));
            };
        }


        return data;
    }



}